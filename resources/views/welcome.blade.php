@extends ('layouts.layout')

@section ('content')

{{-- banner --}}
    <div id="banner">
        <div class="container">
        </div>
    </div>
    {{-- banner --}}

    {{-- blokjes --}}
    <div id="main">
        <div class="container">
            <div class="row">
                <section class="3u">
                    <a href="#" class="image full"><img src="images/NK-Carcassonne-2017.jpg" alt=""></a>
                    <p>Els Bulten (r) en drie onschuldige slachtoffers op het NK Carcassonne 2017</p>
                </section>
                <section class="3u">
                    <a href="#" class="image full"><img src="images/NKCC_Winnaars2019-620x350.jpg" alt=""></a>
                    <p>Alle vier de prijswinnaars van het NK Carcassonne 2019 zijn in de foto hierboven te zien
                        v.l.n.r.: John, Timo, Liesbeth en Dennis. </p>
                </section>
                <section class="3u">
                    <a href="#" class="image full"><img src="images/NK-Carcassonne-2018.jpg" alt=""></a>
                    <p> Het NK Carcassonne 2018. Patrick Bekkenutte was de achtste finalist, ging vervolgens helemaal
                        los in de finalerondes en eindigde het toernooi als de Nederlands Kampioen van 2018. </p>
                </section>
                <section class="3u">
                    <a href="#" class="image full"><img src="images/pics04.jpg" alt=""></a>
                    <p>De vier prijswinnaars van het NK Carcassonne 2016: Arjan Scheulderman, Maureen Adriaans,
                        Gert Versteeg, Tim Tinus</p>
                </section>
            </div>
        </div>
        {{-- /blokjes --}}

        {{-- main --}}
        <div class="container">
            <div class="divider"></div>
            <div class="row">
                <div class="6u">
                    <section>
                        <header>
                            <h2>Welkom bij onze Website!</h2>
                        </header>
                        <p>Bent u een liefhebber van het spel? neem dan gerust een kijkje op deze website! Hier kunt u
                            zich aanmelden voor een persoonlijk account. Indien u geïnteresseerd bent, kunt u zich ook
                        met een druk op de knop aanmelden voor het aankomende toernooi: het NK Carcassonne 2019! Een
                        leuk toernooi vol met heel veel gezellige mensen, een leuke sfeer, en natuurlijk heel veel
                        speel plezier! Dit toernooi is voor jong en oud (minimaal 10 jaar oud) dus wees niet bang
                        en laat ons zien waar je van gemaakt bent. Waar wacht je nog op? Meld je aan!</p>
                    </section>
                </div>
                <div class="6u">
                    <table class="countdownContainer">
                        <tr class="infoTable">
                            <td colspan="4">Het toernooi begint over:</td>
                        </tr>
                        <tr class="info">
                            <td id="days"></td>
                            <td id="hours"></td>
                            <td id="minutes"></td>
                            <td id="seconds"></td>
                        </tr>
                        <tr>
                            <td>Dagen</td>
                            <td>Uren</td>
                            <td>Minuten</td>
                            <td>Seconden</td>
                        </tr>
                    </table>
                </div>
            </div>

            <script type="text/javascript">

                function countdown() {
                    var now = new Date();
                    var eventDate = new Date(2020, 3, 24);

                    var currentTime = now.getTime();
                    var eventTime = eventDate.getTime();

                    var started = eventTime >= currentTime;
                    var remTime = eventTime - currentTime;

                    var s = Math.floor(remTime / 1000);
                    var m = Math.floor(s / 60);
                    var h = Math.floor(m / 60);
                    var d = Math.floor(h / 24) - 30;

                    h %= 24;
                    m %= 60;
                    s %= 60;

                    h = (h < 10) ? "0" + h : h;
                    m = (m < 10) ? "0" + m : m;
                    s = (s < 10) ? "0" + s : s;

                    document.getElementById("days").textContent = d;
                    document.getElementById("hours").textContent = h;
                    document.getElementById("minutes").textContent = m;
                    document.getElementById("seconds").textContent = s;

                    setTimeout(countdown, 1000);

                    if(started) {

                    }
                }

                countdown();

            </script>
        </div>
    </div>
{{-- /main --}}
@endsection
