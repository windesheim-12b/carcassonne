@extends('layouts.layout')

@section('content')

    <table style="width:100%; font-size:20px;" class="TFtable">
        <tr>
            <th>ID</th>
            <th>Username</th>
            <th>Email</th>
            <th>USER</th>
            <th>TOERNOOI LEIDER</th>
            <th>ADMIN</th>
            <th>DELETE PROFILE</th>
        </tr>
        @foreach($users as $u)
            <tr>
                <form action="/admin/{{$u->id}}" method="POST">
                    @csrf
                    @method('PATCH')
                    <td>{{ $u->id}}</td>
                    <td>{{ $u->username }}</td>
                    <td>{{ $u->email }}</td>
                    @if($u->admin == 1)
                        <td>
                            <button style="background-color: lightblue" name="admin" value="1">Make
                                User
                            </button>
                        </td>
                        <td>
                            <button style="background-color: orange" name="admin" value="2">make
                                Contest Leader
                            </button>
                        </td>
                        <td>
                            <button style="background-color: orangered" name="admin" value="3">make
                                Admin
                            </button>
                        </td>

                    @elseif($u->admin == 2)
                        <td>
                            <button style="background-color: green" name="admin" value="1">Make
                                User
                            </button>
                        </td>
                        <td>
                            <button style="background-color: lightblue" name="admin" value="2">make
                                Contest Leader
                            </button>
                        </td>
                        <td>
                            <button style="background-color: orangered" name="admin" value="3">make
                                Admin
                            </button>
                        </td>


                    @elseif($u->admin == 3)
                        <td>
                            <button style="background-color: green" name="admin" value="1">Make
                                User
                            </button>
                        </td>
                        <td>
                            <button style="background-color: orange" name="admin" value="2">make
                                Contest Leader
                            </button>
                        </td>
                        <td>
                            <button style="background-color: lightblue" name="admin" value="3">make
                                Admin
                            </button>
                        </td>

                    @endif
                </form>
                <form method="POST" action="/admin/{{$u->id}}">
                    @csrf
                    @method('DELETE')
                    <td>
                        <button style="background-color: red" name="del" value="delete">Delete Profile
                        </button>
                    </td>
                </form>
            </tr>
        @endforeach
    </table>

@endsection
