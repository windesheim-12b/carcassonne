@extends('layouts.layout')

@section('content')
@if(empty($contest))
    <form method="POST" action="/contestleader">
        @csrf
    <input name="name" type="text" style="width: 400px; height: 50px;">
    <button style="width: 150px; height: 50px;">Make contest</button>
    </form>
    @endif
    <table style="width:100%; font-size:20px;" class="TFtable">
        <tr>
            <th>ID</th>
            <th>Username</th>
            <th>Email</th>
            <th>USER</th>
            <th>TOERNOOI LEIDER</th>
        </tr>
        @foreach($users as $user)
            <tr>
                <form action="/admin/{{$user->id}}" method="POST">
                    @csrf
                    @method('PATCH')
                    <td>{{ $user->id}}</td>
                    <td>{{ $user->username }}</td>
                    <td>{{ $user->email }}</td>
                    @if($user->admin == 1)
                        <td>
                            <button style="background-color: lightblue" name="admin" value="1">Make
                                User
                            </button>
                        </td>
                        <td>
                            <button style="background-color: orange" name="admin" value="2">make
                                Contest Leader
                            </button>
                        </td>
                    @elseif($user->admin == 2)
                        <td>
                            <button style="background-color: green" name="admin" value="1">Make
                                User
                            </button>
                        </td>
                        <td>
                            <button style="background-color: lightblue" name="admin" value="2">make
                                Contest Leader
                            </button>
                        </td>
                    @endif
                </form>
            </tr>
        @endforeach
    </table>
    <div>
    <form method="POST" action="/contestleader/toggle">
        @csrf
        @method('DELETE')
        @php
        $check = App\Contest::all()->first();
        @endphp
        @if(empty($check))
        @elseif($check->active == 1)
        <button style="background-color: orange" name="toggle" value="2">
            Close Contest Joining
        </button>
            @elseif($check->active == 2)
            <button style="background-color: orange" name="toggle" value="1">
                Open Contest Joining
            </button>
            @endif
    </form>
    </div>
    <br/>
@endsection
