@extends('layouts/layout')


@section('content')
    <!-- Main -->
    <div id="main">

        <!-- Main Content -->
        <div class="container">
            <div class="row">

                <div class="7u skel-cell-important">
                    <section>
                        <header>
                            <h2>De inspiratie</h2>
                        </header>
                        <p>
                            Klaus-Jürgen Wrede, de auteur van Carcassonne, bracht eind vorige eeuw een bezoek aan de
                            gelijknamige Franse stad. Hij was daar om inspiratie op te doen voor het schrijven van een
                            roman.
                        </p>
                        <p>
                            En geïnspireerd raakte hij zeker, maar niet voor zijn boek. Klaus-Jürgen had meerdere ijzers
                            in het vuur, want hij hield zich als hobbyist bezig met het ontwerpen van bordspellen.
                            De historische stad en omgeving brachten hem het perfecte thema voor zijn te ontwerpen spel;
                            slechts een half jaar later was Carcassonne klaar en getest. Zijn spelconcept werd meteen
                            opgepikt en in de herfst van 2000 werd het gepresenteerd op de spellenbeurs Spiel in
                            Duitsland.
                        </p>
                        <p>
                            Een jaar later won het op diezelfde beurs de prijs voor Spiel des Jahres, gevolgd door de
                            Deutsche Spielepreis. Deze Duitse dubbel zou het begin betekenen van de zegetocht van
                            Carcassonne. Het spel is met prijzen overladen, kent tal van uitbreidingen en wordt nog
                            altijd veel verkocht.
                        </p>
                    </section>
                    <section>
                        <header>
                            <h2>De bedoeling van het spel</h2>
                        </header>
                        <p>
                            Alle spelers hebben één duidelijk doel bij het bordspel Carcassonne. De gelijknamige stad
                            moet namelijk opgebouwd worden door de deelnemers. Er moet gedacht worden aan kloosters,
                            wegen, weiden en steden. Er moeten zogenaamde horigen ingezet worden en door projecten af te
                            bouwen kunnen er punten gescoord worden. Het draait allemaal op de horigen, want wie de
                            meeste horigen op een afgerond project heeft ontvangt de meeste punten.
                        </p>
                        <p>
                            Toch moet er ook gelet worden op wat voor projecten het zijn, het aantal punten kunnen
                            hierin verschillen. Zodra alle tegels zijn neergelegd is bekend wie de winnaar van het spel
                            Carcassonne zal zijn. Het is een spel met diepgang, waarvoor ook het nodige tactische
                            inzicht nodig is.
                        </p>
                    </section>
                    <section>
                        <header>
                            <h2>Het Materiaal</h2>
                        </header>
                        <p>
                            De tegel met de donkere achterkant is de starttegel. Deze wordt open op tafel gelegd.
                            De overige tegels worden geschud en in meerdere stapels omgekeerd op tafel gelegd.
                            Het score spoor wordt gelegd aan de rand van het speelveld.
                            Iedere speler krijgt 8 horigen van dezelfde kleur en plaatst daarvan 1 horige op het score spoor.
                        </p>
                    </section>

                </div>

                <div id="sidebar1" class="5u">
                    <section>
                        <header>
                            <h2>Video Uitleg</h2>
                        </header>
                        <iframe width="420" height="315"
                                src="https://www.youtube.com/embed/UuMHI2O4S5M">
                        </iframe>
                    </section>
                    <section>
                        <header>
                            <h2>Spel handleiding</h2>
                        </header>
                        <div class="imgContainer" >
                            <img width="300" height="300"
                                 src="https://www.spelregels.eu/wp-content/uploads/2018/01/spelregels-Carcassonne.jpg"
                                 class="vc_single_image-img attachment-full"
                                 srcset="https://www.spelregels.eu/wp-content/uploads/2018/01/spelregels-Carcassonne.jpg
                                 300w, https://www.spelregels.eu/wp-content/uploads/2018/01/spelregels-Carcassonne-150x150.jpg
                                 150w" sizes="(max-width: 300px) 100vw, 300px">
                            <a href="https://www.spelregels.eu/PDF/spelregels-carcassonne.pdf" class="myButton">Download
                            de spelregels</a>
                        </div>
                    </section>

                </div>

            </div>

        </div>
        <!-- /Main Content -->

    </div>

@endsection
