@extends('layouts.layout')

@section('content')
    <link rel="stylesheet" href="/css/form.css">

    <div class="container">
        <form method="POST" action="{{ route('login') }}">
            @csrf

            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Adres') }}</label>

            <div class="col-md-6">
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email"
                       value="{{ old('email') }}" required autocomplete="email" autofocus>

                @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>


            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Wachtwoord') }}</label>

            <div class="col-md-6">
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror"
                       name="password" required autocomplete="current-password">

                @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>


            <div class="form-group row">
                <div class="col-md-6">
                    <div class="form-check">
                        <label class="form-check-label" for="remember">
                            {{ __('Remember Me') }}
                        </label>
                        <input class="form-check-input" type="checkbox" name="remember"
                               id="remember" {{ old('remember') ? 'checked' : '' }}>
                    </div>
                </div>
            </div>

            <input class="send" type="submit" value="Inloggen">
            @if (Route::has('password.request'))
                <a class="btn btn-link" href="{{ route('password.request') }}">
                    {{ __('Wachtwoord vergeten?') }}
                </a>
            @endif

        </form>
    </div>

{{--<div class="wrapper fadeInDown">--}}
{{--    <div id="formContent">--}}

{{--        <form>--}}
{{--            <input type="text" id="login" class="fadeIn second" name="login" placeholder="login">--}}
{{--            <input type="text" id="password" class="fadeIn third" name="login" placeholder="password">--}}
{{--            <input type="submit" class="fadeIn fourth" value="Log In">--}}
{{--        </form>--}}

{{--        <!-- Remind Passowrd -->--}}
{{--        <div id="formFooter">--}}
{{--            <a class="underlineHover" href="#">Forgot Password?</a>--}}
{{--        </div>--}}

{{--    </div>--}}
{{--</div>--}}
@endsection
