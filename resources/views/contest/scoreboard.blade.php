@extends('layouts.layout')

@section('content')
    @if(\App\Contest::first()->round == 8)
        Show Winner! @foreach($participants as $winner)
            {{$winner->name}}
            @endforeach
            @elseif(\App\Contest::first()->round !== 8)

                @if(\App\Contest::first()->round == 0)
                    <div class="card" style="text-align: center"><br/><br/>
                        <div class="card-header row justify-content-center" style="font-size:40px;">
                            De eerste ronde is nog niet begonnen!
                        </div>
                        <br/><br/>
                        @if(Auth::user()->admin == 2)
                            <form action="/scoreboard/start/edit" method="GET">@csrf
                                <button>START DE EERSTE RONDE!</button>
                            </form>
                        @endif
                        @else

                            @if(Auth::user()->admin == 2)
                                <div class="card" style="text-align: center">
                                    <div class="card-header row justify-content-center" style="font-size:20px;">
                                        Scoreboard Toernooi
                                    </div>
                                    <div class=" TFtable">
                                        @php
                                            $num = 1;
                                        @endphp

                                        <div class="card-header row justify-content-center" style="font-size:20px;">
                                            RONDE {{ \App\Contest::first()->pluck('round') }}</div>

                                        <table style="width:100%; font-size:20px;">

                                            <tr>
                                                <th>GROEP</th>
                                                <th>plaats</th>
                                                <th>Username</th>
                                                <th>vorige punten</th>
                                                <th>deze ronde</th>
                                                <th>punten ronde</th>
                                                <th>[ID]</th>
                                            </tr>
                                            @php
                                                $prevround = \App\Contest::first()->round - 1;
                                                if ($prevround == 1){
                                                $pr = 'round1';
                                                }elseif ($prevround == 2){
                                                $pr = 'round2';
                                                }elseif ($prevround == 3){
                                                $pr = 'round3';
                                                }elseif ($prevround == 4){
                                                $pr = 'semi1';
                                                }elseif ($prevround == 5){
                                                $pr = 'semi2';
                                                }elseif ($prevround == 6){
                                                $pr = 'semi3';
                                                }elseif ($prevround == 7){
                                                $pr = 'finale';
                                                }


                                                $thisround = \App\Contest::first()->round ;
                                                if ($thisround == 1){
                                                $gr = 'groupround1';
                                                $ts = 'round1';
                                                }elseif ($thisround == 2){
                                                $gr = 'groupround2';
                                                $ts = 'round2';
                                                }elseif ($thisround == 3){
                                                $gr = 'groupround3';
                                                $ts = 'round3';
                                                }elseif ($thisround == 4){
                                                $gr = 'groupsemi1';
                                                $ts = 'semi1';
                                                }elseif ($thisround == 5){
                                                $gr = 'groupsemi2';
                                                $ts = 'semi2';
                                                }elseif ($thisround == 6){
                                                $gr = 'groupsemi3';
                                                $ts = 'semi3';
                                                }elseif ($thisround == 7){
                                                $gr = 'groupfinale';
                                                $ts = 'finale';
                                                }
                                            $gcount = 0;
                                            @endphp

                                            @foreach($participants as $participant)
                                                <?php $participants = \app\Participants::all()->where($gr, ++$gcount); ?>
                                            <td style="background-color: white;">   <hr style="margin-top: -1px;"><br/></td>
                                            @foreach($participants as $participant)
                                                <tr>
                                                    <td>{{$gcount}}</td>
                                                    <td>{{ $num++ }}</td>
                                                    <td>{{ $participant->name }}</td>
                                                    <td> @if($prevround == 0)Er zijn geen rondes gespeeld @else {{$participant->$pr}} @endif </td>
                                                    <td>{{$participant->$ts}}</td>
                                                    <td>
                                                        <form method="POST"
                                                              action="/scoreboard/{{$participant->id}}">@csrf @method('PATCH')
                                                            <input
                                                                type="number" name="{{ $participant->id }}"
                                                                placeholder="Fill in points">
                                                            <button>Opslaan</button>
                                                        </form>
                                                    </td>
                                                    <td>{{ $participant->id }} </td>
                                                </tr>
                                                @endforeach
                                            @endforeach
                                        </table>
                                    </div>
                                    @if($thisround < 7)
                                        <form action="/scoreboard/start/edit" method="GET">@csrf
                                            <button style="background-color: orange;">Beëindig ronde</button>
                                        </form>
                                    @else
                                        <form action="/scoreboard/start/edit" method="GET">@csrf
                                            <button style="background-color: orange;">Laat de winnaar zien!</button>
                                        </form>
                                    @endif
                                </div>




                @else

                                <div class="card" style="text-align: center">
                                    <div class="card-header row justify-content-center" style="font-size:20px;">
                                        Scoreboard Contest
                                    </div>
                                    <div class=" TFtable">
                                        @php
                                            $num = 1;
                                        @endphp

                                        <div class="card-header row justify-content-center" style="font-size:20px;">
                                            ROUND {{ \App\Contest::first()->pluck('round') }}</div>

                                        <table style="width:100%; font-size:20px;">

                                            <tr>
                                                <th>GROEP</th>
                                                <th>plaats</th>
                                                <th>Username</th>
                                                <th>vorige punten</th>
                                                <th>deze ronde</th>
                                                <th>[ID]</th>
                                            </tr>
                                            @php
                                                $prevround = \App\Contest::first()->round - 1;
                                                if ($prevround == 1){
                                                $pr = 'round1';
                                                }elseif ($prevround == 2){
                                                $pr = 'round2';
                                                }elseif ($prevround == 3){
                                                $pr = 'round3';
                                                }elseif ($prevround == 4){
                                                $pr = 'semi1';
                                                }elseif ($prevround == 5){
                                                $pr = 'semi2';
                                                }elseif ($prevround == 6){
                                                $pr = 'semi3';
                                                }elseif ($prevround == 7){
                                                $pr = 'finale';
                                                }


                                                $thisround = \App\Contest::first()->round ;
                                                if ($thisround == 1){
                                                $gr = 'groupround1';
                                                $ts = 'round1';
                                                }elseif ($thisround == 2){
                                                $gr = 'groupround2';
                                                $ts = 'round2';
                                                }elseif ($thisround == 3){
                                                $gr = 'groupround3';
                                                $ts = 'round3';
                                                }elseif ($thisround == 4){
                                                $gr = 'groupsemi1';
                                                $ts = 'semi1';
                                                }elseif ($thisround == 5){
                                                $gr = 'groupsemi2';
                                                $ts = 'semi2';
                                                }elseif ($thisround == 6){
                                                $gr = 'groupsemi3';
                                                $ts = 'semi3';
                                                }elseif ($thisround == 7){
                                                $gr = 'groupfinale';
                                                $ts = 'finale';
                                                }
                                            $gcount = 0;
                                            @endphp

                                            @foreach($participants as $participant)
                                                <?php $participants = \app\Participants::all()->where($gr, ++$gcount); ?>
                                                <td style="background-color: white;">   <hr style="margin-top: -1px;"><br/></td>
                                                @foreach($participants as $participant)
                                                    <tr>
                                                        <td>{{$gcount}}</td>
                                                        <td>{{ $num++ }}</td>
                                                        <td>{{ $participant->name }}</td>
                                                        <td> @if($prevround == 0)no rounds
                                                            played @else {{$participant->$pr}} @endif </td>
                                                        <td>{{$participant->$ts}}</td>
                                                        <td>{{ $participant->id }} </td>
                                                    </tr>
                                                @endforeach
                                            @endforeach
                                        </table>
                                    </div>
                                </div>


                @endif




            @endif

          @endif
@endsection
