@extends('layouts.layout')

@section('content')
    <body>
    <br/>
    <div class="card shadow mb-5" style="font-size: 20px; text-align: center;">
        @if(empty($toggle))
            Er is nog geen toernooi begonnen!
        @elseif($toggle->active == 2)
            @if($c !== '')
                Toernooi is gesloten! Wilt u nog steeds verlaten?<br/>
                <form action="/join/{{ Auth::user()->id }}" method="POST">
                    @method('DELETE')
                    @csrf
                    <button>Verlaat het toernooi</button>
                    <br/>
                    <p style="color: orange">WAARSCHUWING!</p>
                    <p> U kunt niet opnieuw aanmelden na het verlaten.</p>
                </form>
    </div>
    @else
        Toernooi is gesloten! U kunt zich niet meer aanmelden.
    @endif
    @elseif($c == '')
        <div class="card-header py-3">
            <h2 style="font-size: 40px;" class="text-primary font-weight-bold m-0">Wilt u zich aanmelden voor het toernooi?</h2>
        </div>
        <div class="text-primary font-weight-bold m-0 btn-sm">

            <div class="card-body">
                <h1> Doe mee aan het toernooi!</h1>
                <form action="/join" method="POST">
                    @csrf
                    <button>JOIN!</button>
                    <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                </form>
                @elseif($c !== '')
                    <div class="card-body text-center">
                        <h2 style="font-size: 40px;">U bent geregistreerd!</h2><br>
                        Wilt u het toernooi verlaten?<br/>
                        <form action="/join/{{ Auth::user()->id }}" method="POST">
                            @method('DELETE')
                            @csrf
                            <button>Verlaat het toernooi!</button>
                        </form>
                    </div>
                @endif


                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>U bent al geregistreerd met dit account!</li>
                            @endforeach
                        </ul>
                    </div>

            </div>
            @endif

        </div>
        </div><br/>
    </body>

@endsection
