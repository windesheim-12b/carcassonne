@extends('layouts/layout')

@section('content')
    <link rel="stylesheet" href="/css/form.css">
<div class="container">
    <h1 style="padding-left: 27px; font-size: 50px"><br/>Neem contact met ons op</h1><br/>
    <p style="padding-left: 27px">Heb je een vraag? Stuur het naar ons toe. Wij proberen binnen 48 uur te antwoorden!</p>
</div>
    <div style="text-align: center;">

        <form action="https://formsubmit.co/wfflix.info@gmail.com" method="POST">
            @csrf
                <input  type="text" name="name"  placeholder="Naam" required><br>
                <input type="email" name="email"  placeholder="E-mail" required><br>
                <input  type="text" name="subject"  placeholder="Onderwerp" required>
            <textarea placeholder="Typ hier uw bericht..."  name="message" rows="10" required></textarea>
            <input class="send" type="submit" value="Verzend!">
        </form>
    </div>

@endsection
