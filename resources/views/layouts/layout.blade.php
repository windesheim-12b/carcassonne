<!DOCTYPE HTML>
<html>
<head>
    <title>Carcassonne</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>
    <link href='http://fonts.googleapis.com/css?family=Questrial' rel='stylesheet' type='text/css'>
    <!--[if lte IE 8]>
    <link href="/maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="/maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="/cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="/js/html5shiv.js"></script><![endif]-->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="/js/skel.min.js"></script>
    <script src="/js/skel-panels.min.js"></script>
    <script src="/js/init.js"></script>
        <link rel="stylesheet" href="/css/scoreboard.css">
        <link rel="stylesheet" href="/css/skel-noscript.css">
        <link rel="stylesheet" href="/css/style.css">
        <link rel="stylesheet" href="/css/style-desktop.css">
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="/css/ie/v8.css"/><![endif]-->
    <!--[if lte IE 9]>
    <link rel="stylesheet" href="/css/ie/v9.css"/><![endif]-->
</head>
<body class="homepage">

<!-- Header -->
<div id="header">
    <div id="logo-wrapper">
        <div class="container">

            <!-- Logo -->
            <div id="logo">
                <h1><a href="/">Carcassonne</a></h1>
                <!-- Authentication Links -->
                @guest
                    <span class="row">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            @if (Route::has('register'))
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                          </span>
                @endif
                @else
                    <span class="row">
                        <a class="nav-link" href="/account">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                            <a class="nav-link" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                {{ __('Log uit') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                    </span>
                @endguest
            </div>

        </div>
    </div>
    <div class="container">
        <!-- Nav -->
        <nav id="nav">
            <ul>
                <li class="{{ Request::is("/") ? 'active' : ''}} "><a href="\home">Homepagina</a></li>
                <li class="{{ Request::is("about") ? 'active' : ''}}"><a href="\about">Over</a></li>
                <li class="{{ Request::is("contact") ? 'active' : ''}}"><a href="\contact">Contact</a></li>
                @guest

                @else
                    <li class="{{ Request::is('scoreboard') ? 'active' : ''}} "><a href="\scoreboard">Toernooi Statistieken</a></li>
                    @if(Auth::User()->admin ==  3)
                        <li class="{{ Request::is("admin") ? 'active' : ''}}"><a href="\admin">Admin</a></li>
                        @endif
                        @if(Auth::User()->admin ==  2)
                            <li class="{{ Request::is("contestleader") ? 'active' : ''}}"><a href="\contestleader">Toernooi Leider</a></li>
                        @endif
                    <li class="{{ Request::is("account") ? 'active' : ''}}"><a href="\account">Account</a></li>
                    <li class="{{ Request::is("join") ? 'active' : ''}}"><a href="\join">Aanmelden</a></li>

                @endguest
            </ul>
        </nav>
    </div>
</div>
<!-- Header -->
<div>
@yield('content')
</div>
<!-- Footer -->
<div id="footer">
    <div class="container">
        <div class="row">
            <div class="6u">
                <section>
                    <header>
                        <h2>Carcassonne</h2>
                    </header>
                    <a href="#" class="image full"><img src="images/carcassonne_01.jpg" alt=""></a>
                    <p>Dit project is gemaakt door Team 12A van het Windesheim Flevoland.</p>
                </section>
            </div>
            <div id="fbox1" class="3u">
                <section>
                    <header>
                        <h2>Varianten en uitbreidingen</h2>
                    </header>
                    <ul class="default">
                        <li class="fa fa-angle-right"><a
                                href="https://nl.wikipedia.org/wiki/Carcassonne_(spel)#1._Carcassonne:_De_Uitbreiding">De
                                Uitbreiding</a></li>
                        <li class="fa fa-angle-right"><a
                                href="https://nl.wikipedia.org/wiki/Carcassonne_(spel)#2._Carcassonne:_Kooplieden_en_Bouwmeesters">Kooplieden
                                en Bouwmeesters</a></li>
                        <li class="fa fa-angle-right"><a
                                href="https://nl.wikipedia.org/wiki/Carcassonne_(spel)#3._Carcassonne:_De_Draak,_de_Fee_en_de_Jonkvrouw">De
                                Draak, de Fee en de Jonkvrouw</a></li>
                        <li class="fa fa-angle-right"><a
                                href="https://nl.wikipedia.org/wiki/Carcassonne_(spel)#4._Carcassonne:_De_Toren">De
                                Toren</a></li>
                        <li class="fa fa-angle-right"><a
                                href="https://nl.wikipedia.org/wiki/Carcassonne_(spel)#5._Carcassonne:_Burgemeesters_en_Abdijen">Burgemeesters
                                en Abdijen</a></li>
                        <li class="fa fa-angle-right"><a
                                href="https://nl.wikipedia.org/wiki/Carcassonne_(spel)#6._Carcassonne:_Graaf,_Koning_en_consorten">Graaf,
                                Koning en consorten</a></li>
                    </ul>
                </section>
            </div>
            <div id="fbox2" class="3u">
                <section>
                    <header>
                        <h2>Klantenservice</h2>
                    </header>
                    <ul class="default">
                        <li class="fa fa-angle-right"><a href="#">0900 - 999 0000</a></li>
                        <li class="fa fa-angle-right"><a href="#">Integer rutrum nisl in mi</a></li>
                        <li class="fa fa-angle-right"><a href="#">Etiam malesuada rutrum enim</a></li>
                        <li class="fa fa-angle-right"><a href="#">Aenean elementum facilisis ligula</a></li>
                        <li class="fa fa-angle-right"><a href="#">Ut tincidunt elit vitae augue</a></li>
                        <li class="fa fa-angle-right"><a href="#">Sed quis odio sagittis leo vehicula</a></li>
                    </ul>
                </section>
            </div>
        </div>


    </div>
</div>
<!-- /Footer -->

<!-- Copyright -->
<div id="copyright">
    <div class="container">
        <section>
            Customisation: <a href="#">Team 12A</a> van het <a href="https://www.windesheimflevoland.nl/">Windesheim
                Flevoland</a>
        </section>
    </div>
</div>

</body>
</html>
