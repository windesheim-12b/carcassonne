<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Participants;
class Contest extends Model
{
    protected $guarded = [];

    public function users()
    {
        return $this->hasMany('App\Participants', 'user_id');
    }
}
