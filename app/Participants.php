<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Contest;
class Participants extends Model
{
    protected $guarded = [];
    public function contest()
    {
        return $this->belongsTo('App\Participants', 'contest_id');
    }
    public function users()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
