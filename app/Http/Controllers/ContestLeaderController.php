<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Contest;
use App\User;
use Illuminate\Http\Request;

class ContestLeaderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contest = trim(Contest::all()->first(), '[]');
        $users = User::all()->whereIn('admin', [1,2]);
        return view('/contestleader/home', compact('users', 'contest'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'name' => ['required'],
            ]);
        $new = new Contest;
        $new->name = request('name');
        $new->save();
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Admin $admin
     * @return \Illuminate\Http\Response
     */
    public function show(Admin $admin)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Admin $admin
     * @return \Illuminate\Http\Response
     */
    public function edit(Admin $admin)
    {
        dd('test');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Admin $admin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user, $id)
    {
        $user = User::find($id);
        $user->update([
            $user->admin = request('admin'),
        ]);
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Admin $admin
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, User $user, $id)
    {
    foreach (Contest::all() as $per){
        Contest::find($per->id)->update([
            'active' => request('toggle')
        ]);
    }
    return back();
    }
}
