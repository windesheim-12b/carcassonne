<?php

namespace App\Http\Controllers;

use App\Participants;
use App\ScoreBoard;
use App\Contest;
use Illuminate\Http\Request;

class ScoreBoardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Contest $contest, Participants $participants)
    {
        $round = Contest::first();
        if (empty($round)) {
            $r = 'round1';
        } elseif ($round->round == 0) {
            $r = 'round1';
        } elseif ($round->round == 1) {
            $r = 'round1';
        } elseif ($round->round == 2) {
            $r = 'round2';
        } elseif ($round->round == 3) {
            $r = 'round3';
        } elseif ($round->round == 4) {
            $r = 'semi1';
        } elseif ($round->round == 5) {
            $r = 'semi2';
        } elseif ($round->round == 6) {
            $r = 'semi3';
        } elseif ($round->round == 7) {
            $r = 'finale';
        } elseif ($round->round == 8) {
            $r = 'finale';
        }
        if (empty(Contest::first())){
            return back();
        }
        $participants = Participants::all()->where('ingame', $round->round)->sortByDesc($r);
        $contest = Contest::all()->first();
        return view('/contest/scoreboard', compact('participants', 'contest'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\ScoreBoard $scoreBoard
     * @return \Illuminate\Http\Response
     */
    public function show(ScoreBoard $scoreBoard)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\ScoreBoard $scoreBoard
     * @return \Illuminate\Http\Response
     */
    public function edit(ScoreBoard $scoreBoard)
    {
        $num = 0;
        $group = 1;
        Contest::first()->update([
            'round' => Contest::first()->round + 1
        ]);
        $round = Contest::first();
// rules for start round 1 ROUND1
        if ($round->round == 1) {
            $SQLgroup = 'groupround1';
            foreach (Participants::all()->where('ingame', 1) as $per) {
                if (++$num !== 4) {
                    $per->update([
                        'groupround1' => $group
                    ]);
                } else {
                    $per->update([
                        'groupround1' => $group++
                    ]);
                    $num = 0;
                }
            }
        } // rules for start round 2 ROUND2
        elseif ($round->round == 2) {
            $count = 0;
            foreach (Participants::all() as $idc) {
                $groupmembers = Participants::all()->where('groupround1', ++$count)->sortByDesc('groupround1');
                foreach ($groupmembers as $mem) {
                    $all = array_sum($groupmembers->pluck('round1')->toArray());
                    if ($all == 0) {
                        $calc = 0;
                    } else {
                        $calc = 100 / $all * $mem->round1;
                    }
                    $mem->update([
                        'percentage1' => $calc
                    ]);
                }
            }
            $SQLgroup = 'groupround2';
            foreach (Participants::all()->where('ingame', 1)->sortByDesc('percentage1') as $per) {
                $per->update([
                    'ingame' => 2
                ]);
                if (++$num !== 4) {
                    $per->update([
                        'groupround2' => $group
                    ]);
                } else {
                    $per->update([
                        'groupround2' => $group++
                    ]);
                    $num = 0;
                }
            }
        }// rules for start round 3 ROUND3
        elseif ($round->round == 3) {
            $count = 0;
            foreach (Participants::all() as $idc) {
                $groupmembers = Participants::all()->where('groupround2', ++$count)->sortByDesc('groupround2');
                foreach ($groupmembers as $mem) {
                    $all = array_sum($groupmembers->pluck('round2')->toArray());
                    if ($all == 0) {
                        $calc = 0;
                    } else {
                        $calc = 100 / $all * $mem->round2;
                    }
                    $mem->update([
                        'percentage2' => $calc,
                        'percentageadd2' => ($mem->percentage1 + $calc) / 2
                    ]);
                }
            }
            $SQLgroup = 'groupround3';
            foreach (Participants::all()->where('ingame', 2)->sortByDesc('percentageadd2') as $per) {
                $per->update([
                    'ingame' => 3
                ]);
                if (++$num !== 4) {
                    $per->update([
                        'groupround3' => $group
                    ]);
                } else {
                    $per->update([
                        'groupround3' => $group++
                    ]);
                    $num = 0;
                }
            }
        }// rules for start round 4 SEMI1
        elseif ($round->round == 4) {
            $count = 0;
            foreach (Participants::all()->where('ingame', 3)->sortByDesc('round3') as $per) {
                if (++$count <= 16) {
                    $per->update([
                        'ingame' => 4
                    ]);
                }
            }
            foreach (Participants::all()->where('ingame', 4)->sortByDesc('round3') as $per) {
                if (++$num !== 2) {
                    $per->update([
                        'groupsemi1' => $group
                    ]);
                } else {
                    $per->update([
                        'groupsemi1' => $group++
                    ]);
                    $num = 0;
                }
            }
        } // rules for start round 5 SEMI2
        elseif ($round->round == 5) {
            $count = 0;
            foreach (Participants::all()->where('ingame', 4) as $per) {
                $disq = 0;
                $groupmembers = Participants::all()->where('groupsemi1', ++$count)->sortByDesc('semi1');
                foreach ($groupmembers as $mem) {
                    if (++$disq == 1) {
                        $mem->update([
                            'ingame' => 5
                        ]);
                    }
                }
            }
            foreach (Participants::all()->where('ingame', 5)->sortByDesc('semi1') as $per) {
                if (++$num !== 2) {
                    $per->update([
                        'groupsemi2' => $group
                    ]);
                } else {
                    $per->update([
                        'groupsemi2' => $group++
                    ]);
                    $num = 0;
                }
            }
        }// rules for start round 6 SEMI3
        elseif ($round->round == 6) {
            $count = 0;
            foreach (Participants::all()->where('ingame', 5) as $per) {
                $disq = 0;
                $groupmembers = Participants::all()->where('groupsemi2', ++$count)->sortByDesc('semi2');
                foreach ($groupmembers as $mem) {
                    if (++$disq == 1) {
                        $mem->update([
                            'ingame' => 6
                        ]);
                    }
                }
            }
            foreach (Participants::all()->where('ingame', 6)->sortByDesc('semi2') as $per) {
                if (++$num !== 2) {
                    $per->update([
                        'groupsemi3' => $group
                    ]);
                } else {
                    $per->update([
                        'groupsemi3' => $group++
                    ]);
                    $num = 0;
                }
            }
        }// rules for start round 7 FINALE
        elseif ($round->round == 7) {
            $count = 0;
            foreach (Participants::all()->where('ingame', 6) as $per) {
                $disq = 0;
                $groupmembers = Participants::all()->where('groupsemi3', ++$count)->sortByDesc('semi3');
                foreach ($groupmembers as $mem) {
                    if (++$disq == 1) {
                        $mem->update([
                            'ingame' => 7
                        ]);
                    }
                }
            }
            foreach (Participants::all()->where('ingame', 7)->sortByDesc('semi3') as $per) {
                if (++$num !== 2) {
                    $per->update([
                        'groupfinale' => $group
                    ]);
                } else {
                    $per->update([
                        'groupfinale' => $group++
                    ]);
                    $num = 0;
                }
            }
        }// rules for start round 8 finale RESULT
        elseif ($round->round == 8) {
            $count = 0;
            $disq = 0;
            foreach (Participants::all()->where('ingame', 7) as $per) {
                $groupmembers = Participants::all()->where('groupsemi3', ++$count)->sortByDesc('semi3');
                foreach ($groupmembers as $mem) {
                    if (++$disq == 1) {
                        $mem->update([
                            'ingame' => 8
                        ]);
                    }
                }
            }
        }

        if ($round == 'round1' || $round == 'round2' || $round == 'round3') {
            $size = Participants::all()->where($SQLgroup, $group)->sortByDesc($SQLgroup)->toArray();
            if (count(Participants::all('id')->toArray()) == 1) {

            } else {
                if (count($size) == 1) {
                    Participants::all()->where($SQLgroup, --$group)->last()->update([
                        $SQLgroup => ++$group
                    ]);
                    Participants::all()->where($SQLgroup, --$group - 1)->last()->update([
                        $SQLgroup => ++$group
                    ]);
                } elseif (count($size) == 2) {
                    Participants::all()->where($SQLgroup, --$group)->last()->update([
                        $SQLgroup => ++$group
                    ]);
                }
            }
        }

        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\ScoreBoard $scoreBoard
     * @return \Illuminate\Http\Response
     */
    public
    function update($id, Request $request, ScoreBoard $scoreBoard)
    {
        $round = Contest::first();
        if ($round->round == 1) {
            Participants::find($id)->update([
                'round1' => request($id)
            ]);
        } elseif ($round->round == 2) {
            Participants::find($id)->update([
                'round2' => request($id)
            ]);
        } elseif ($round->round == 3) {
            Participants::find($id)->update([
                'round3' => request($id)
            ]);
        } elseif ($round->round == 4) {
            Participants::find($id)->update([
                'semi1' => request($id)
            ]);
        } elseif ($round->round == 5) {
            Participants::find($id)->update([
                'semi2' => request($id)
            ]);
        } elseif ($round->round == 6) {
            Participants::find($id)->update([
                'semi3' => request($id)
            ]);
        } elseif ($round->round == 7) {
            Participants::find($id)->update([
                'finale' => request($id)
            ]);
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\ScoreBoard $scoreBoard
     * @return \Illuminate\Http\Response
     */
    public
    function destroy(ScoreBoard $scoreBoard)
    {
        //
    }
}
