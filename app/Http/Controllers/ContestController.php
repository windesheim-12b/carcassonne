<?php

namespace App\Http\Controllers;

use App\Contest;
use App\Participants;
use Illuminate\Http\Request;
use Auth;
use function Composer\Autoload\includeFile;

class ContestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $toggle = Contest::all()->first();
        $c = trim(Participants::all('user_id')->where('user_id', Auth::user()->id), '\[{"user_id":}]');
        return view('/contest/join', compact('c', 'toggle'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        request()->validate([
            'user_id' => ['required', 'unique:Participants'],
        ]);

        if (Auth::user()->id == request('user_id')) {
            Participants::create([
                'user_id' => Auth::user()->id,
                'contest_id' => Contest::all()->first()->id,
                'name' => Auth::user()->username
            ]);
            return redirect('/join');
        } else {
            return redirect('/join');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Contest $contest
     * @return \Illuminate\Http\Response
     */
    public function show(Contest $contest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Contest $contest
     * @return \Illuminate\Http\Response
     */
    public function edit(Contest $contest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Contest $contest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contest $contest)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Contest $contest
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contest $contest)
    {

        $a = Auth::user()->id;
        $b = Participants::all()->where('user_id',  $a)->pluck('id');
        $c = trim($b, '/ => []' );
        Participants::findorfail($c)->delete();

        return redirect('/join');
    }
}
