<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/about', function () {
    return view('about');
});

Route::get('/contact', function () {
    return view('contact');
});


Route::get('/tournament_statistics', function () {
    return view('tournament_statistics');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/account', 'AccountController@index');

// JOIN ROUTES
Route::resource('/join', 'ContestController');
// SCOREBOARD ROUTES
Route::resource('/scoreboard', 'ScoreBoardController');
// ADMIN ROUTES
Route::resource('/admin', 'AdminController');
// CONTEST LEADER ROUTES
Route::resource('/contestleader', 'ContestLeaderController');

