<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParticipantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('participants', function (Blueprint $table) {
            $table->bigIncrements('id')->unique();
            $table->unsignedBigInteger('user_id')->unique();
            $table->string('name');
            $table->unsignedBigInteger('contest_id');
            $table->string('ingame')->default(1);
            $table->string('round1')->default(0);
            $table->string('percentage1')->default(0);
            $table->string('groupround1')->default(0);
            $table->string('round2')->default(0);
            $table->string('percentage2')->default(0);
            $table->string('groupround2')->default(0);
            $table->string('percentageadd2')->default(0);
            $table->string('round3')->default(0);
            $table->string('groupround3')->default(0);
            $table->string('semi1')->default(0);
            $table->string('groupsemi1')->default(0);
            $table->string('semi2')->default(0);
            $table->string('groupsemi2')->default(0);
            $table->string('semi3')->default(0);
            $table->string('groupsemi3')->default(0);
            $table->string('finale')->default(0);
            $table->string('groupfinale')->default(0);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');

            $table->foreign('contest_id')->references('id')->on('contests');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('participants');
    }
}
